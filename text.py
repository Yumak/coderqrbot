string = {
    "lang": "Select a language from the list:\nВыберите язык из списка:",
    "english": {
        "message": {
            "restart": "The bot restarted.",
            "cancel": "Action canceled\nMain menu👇🏽",
            "back": "Main menu👇🏽",
            "support": "Do you have any questions? Or do you have a problem?\n"
                       "Write messages here, they will be sent to our administrators, "
                       "and they will respond to you soon.\nTo exit this section, write me 'Back', "
                       "or press the 'Back' button on your keyboard.",
            "help": "*QR-coder - bot for creating and reading qr-codes.*\n"
                    "Navigation through the bot's functionality is simple, using the navigation buttons.\n"
                    "In the *normal* version, the following configurations are available to the user:\n"
                    "🔘create up to 5 regular qr codes per day5️⃣,\n"
                    "🔘create up to 3 qr-codes with your logo per day3️⃣,\n"
                    "🔘read up to 5 qr-codes per day5️⃣.\n"
                    "🔑In the *pro-account* version, the user can:\n"
                    "🔘unlimited access to all services provided by the bot♾,\n"
                    "🔘ability to change the colors of qr codes🔻,\n"
                    "🔘no Intrusive advertising🔇.\n"
                    "You can get a *pro-account* by paying for this service in the `Balance` section, "
                    "or enter a promo code.\n"
                    "😎*Enjoy using the bot*😎",
            "newqr": "please Send me the text that needs to be encrypted into a qr-code.\n"
                     "The colors can be changed in the settings.\n"
                     "Available here: ",
            "newqrrez": {
                True: "the QR-code was created using the @coderqrbot bot",
                False: "You have run out of free attempts for today, please try again tomorrow, "
                       "or buy a pro-account in the 'Balance ' section",
            },
            "newqrlogo": "☝Current logo.\n"
                         "Please send me the text that needs to be encrypted into a qr-code.\n"
                         "You can change the colors in the settings.\n"
                         "Available here: ",
            "changelogo": "☝Current logo.\n"
                          "Send me a photo of the new logo.",
            "readqr": "Send me a photo of the QR-code, I will try to decipher it.\n"
                      "Available attempts: ",
            "readqrrez": {
                "text": "image decoding Result:\n",
                False: "Failed to decrypt your qr-code.",
                "cancel": "You have run out of free attempts for today, please try again tomorrow, "
                          "or buy a pro-account in the `Balance` section",
            },
            "setting": "Select which settings you want to change from the menu.",
            "color": {
                True: "Select the color you want to change from the menu.",
                False: "The ability to select QR-code colors in your account is disabled.\n"
                       "To enable this feature, buy a pro-account in the `Balance` section."
            },
            "inline": {
                True: "Inline mode is enabled in your account.",
                False: "Inline mode is disabled in your account.\n"
                       "To enable this feature, buy a pro-account in the `Balance` section.\n"
                       "Inline mode allows you to create qr-codes from other chats."
            },
            "advertising": {
                True: "Ads are currently enabled.\n"
                      "To disable ads, buy a pro-account in the `Balance` section.",
                False: "Ads are disabled in your account."
            },
            "balance": {
                True: "Online mode in development mode.",
                False: "Online mode in development mode.",
                #True: "Pro-account is activated.",
                #False: "*The Pro-account is disabled*.\n"
                #       "If you have a promo code, click on the corresponding button in the menu.\n"
                #       "To buy a pro-account, or find out the price, click on 'Buy a pro-account'.\n"
                #       "If you paid for a pro-account but it wasn't activated, click 'Update'. "
                #       "If it doesn't help, please contact our support service."
            },
            "advertising": {
                True: "At the moment the is included.\nTo disable ads, "
                      "buy a pro-account in the `Balance` section.",
                False: "Ads are disabled in your account."
            },
            "balance": {
                "cancel": "Hi, the paid time of the pro-account is over, now you have access to basic functions."
                          "To continue using unlimited access, pay for a pro-account in the `Balance` section, "
                          "or enter the promo code in the same section.",
                True: "🥳Pro-account activated.🥳 Paid days: ",
                False: "😒*A Pro-account is disabled.*😒\nIf you have a promo code, click on the corresponding button "
                       "in the menu.\nTo buy a pro-account, or find out the price, click on ' Buy a pro-account'.\n"
                       "If you paid for a pro-account but it wasn't activated, click 'Update'. "
                       "If it doesn't help, please contact our support service.\n"
                       "If you want to get a pro-account on special terms(for a week, with a discount), "
                       "please contact our support service with an explanation of why we can give you a pro-account "
                       "on special terms. "
            },
            "buy_pro": "Buy a pro-account",
            "extend_pro": "Extend your pro-account",
            "promotion": "Send me the promo code✏(12 characters) without extra characters.\n"
                         "If you want to give a pro-account to a friend, you can buy a promo code "
                         "for 1 month or 1 year. To do this, click the appropriate buttons in the menu.\n"
                         "The promo code is non-refundable and cannot be exchanged. it is valid for 30 days "
                         "from the date of purchase.",
            "promo": {
                "newpromotion": "Promo code created: ",
                "buymonth": "🎁You bought a promo code for 1 month.\nYour promo code: ",
                "buyyear": "🎁You bought a promo code for 1 year.\nYour promo code: ",
                True: "🎉The promo code was successfully applied.🎉",
                False: "❌The promo code is invalid.❌\n"
                       "If you are sure that the promo code is valid, or you want to find out the reason why the "
                       "promo code is invalid, please contact our support service.",
                "used1": "used your promo code:",
                "used2": "and it is no longer valid.",
                "cancel": "A month has passed since you purchased the promo code. unfortunately, "
                          "it is no longer valid, since no one has used it for a month.\nPromo code:",
            },
            "buymonth": {
                "title": "1 month pro-account",
                "description": "Pay for 1 month pro-account, you unlock all possible, will not come is, "
                               "the description of qr-codes are not advertising this bot, as well as extra buttons "
                               "for a whole month.",
                "price": "1 month subscription",
            },
            "buyyear": {
                "title": "1 year pro-account",
                "description": "Pay for 1 year pro-account, you unlock all possible, will not come is, "
                               "the description of qr-codes will not be advertising this bot, as well as extra "
                               "buttons for a year.",
                "price": "1 year subscription",
            },
            "admin_panel": "You are logged in to the admin panel, select what you want to do from the menu.",
            "allpost": "Enter the text to send to all users.",
            "nopropost": "Enter the text to send to all users except pro-account owners.",
            "oneuserpost": "Send me the chat id of the user to send the post to.",
            "oneuserpost_text": "Enter the text to send to the user:\n",
            "statistic": "Bot user statistics:",
            "background": "Select a color from the menu.\n"
                          "Current background color:",
            "module": "Select a color from the menu.\n"
                      "Current qr-code color:",
            "backgroundset": "The selected background color:",
            "moduleset": "QR-code color selected:",
            "newpromotion": "Send me the number of days to create a promo code for.",

        },
        "button": {
            "back": "🔙Back",
            "cancel": "❌Cancel",
            "balance": "💳Balance",
            "newqr": "➕New QR-code",
            "readqr": "📝Decipher QR-code",
            "newqrlogo": "🖼New QR-code with logo",
            "setting": "⚙Setting",
            "support": "❗Support",
            "help": "ℹHelp",
            "language": "🇺🇸Language",
            "color": "🔲Default color",
            "inline": "📚Inline mode",
            "advertising": "📣Advertising",
            "buypro": "💰🔑Buy pro-account",
            "update": "🔄Update",
            "promotion": "🎁Enter a promo-code",
            "extendpro": "💰Extend pro-account",
            "buymonth": "📆Buy 1 month",
            "buyyear": "📅Buy 1 year",
            "buypromotionmonth": "📆Buy a promo-code for 1 month",
            "buypromotionyear": "📅Buy a promo-code for 1 year",
            "adminpanel": "🔐Admin panel",
            "allpost": "Опубликовать пост всем",
            "nopropost": "Опубликовать пост не pro",
            "oneuserpost": "Отправить пост одному",
            "statistic": "Получить статистику",
            "newpromotion": "Создать промокод",
            "background": "◽Background color",
            "module": "▪Color qr-code",
            "logo": "🖼Change logo",

        },

    },
    "russian": {
        "message": {
            "restart": "Бот перезапущен.",
            "cancel": "Действие отменено\nГлавное меню👇🏽",
            "back": "Главное меню👇🏽",
            "support": "У вас возникли вопросы? Или у вас появилась проблема?\n"
                       "Пиши сюда сообщения, они будут переданы нашим администраторам, и они "
                       "в ближайшее время вам ответят.\nДля выхода из этого раздела напишите мне 'Назад', "
                       "или нажмите на клавиатуре кнопку 'Назад'.",
            "help": "*QR-coder - бот для создания и чтения qr-кодов.*\n"
                    "Навигация по функционалу бота простая, осуществляется при помощи навигационных кнопок.\n"
                    "В *обычной* версии пользователю доступны следующие конфигурации:\n"
                    "🔘создание до 5 обычных qr-кодов в день5️⃣,\n"
                    "🔘создание до 3 qr-кодов с логотипом в день3️⃣,\n"
                    "🔘чтение до 5 qr-кодов в день5️⃣.\n"
                    "🔑В версии *pro-аккаунта* пользователю доступно:\n"
                    "🔘безлимит на все услуги предостовляемые ботом♾,\n"
                    "🔘возможность менять цвета qr-кодов🔻,\n"
                    "🔘отсутсвие навязчивой рекламы🔇.\n"
                    "Получить *pro-аккаунт* можно оплатив данную услугу в разделе `Баланс`, либо ввести промокод.\n"
                    "😎*Приятного пользования ботом*😎",
            "newqr": "Пришлите мне пожалуйста текст который необходимо зашифровать в qr-код.\n"
                     "Цвета можно поменять в настройках\n"
                     "Доступно попыток: ",
            "newqrrez": {
                True: "QR-код создан с помощью бота @coderqrbot",
                False: "У вас закончились бесплатные попытки на сегодня, пожалуйста попробуйте еще раз завтра, "
                       "или купите pro-аккаунт в разделе `Баланс`",
            },
            "newqrlogo": "☝Текущий логотип.\n"
                         "Пришлите мне пожалуйста текст который необходимо зашифровать в qr-код.\n"
                         "Цвета можно поменять в настройках\n"
                         "Доступно попыток: ",
            "changelogo": "☝Текущий логотип.\n"
                          "Пришлите мне фото нового логотипа.",
            "readqr": "Пришлите мне фотографию QR-кода, я попробую его расшифровать.\n"
                      "Доступно попыток: ",
            "readqrrez": {
                "text": "Результат декодирования изображения:\n",
                False: "Неудалось расшифровать ваш qr-код.",
                "cancel": "У вас закончились бесплатные попытки на сегодня, пожалуйста попробуйте еще раз завтра, "
                          "или купите pro-аккаунт в разделе `Баланс`",
            },
            "setting": "Выберите из меню какие настройки вы желаете изменить.",
            "color": {
                True: "Выберите в меню, какой цвет желаете поменять.",
                False: "Возможность выбора цветов qr-кодов в вашем аккаунте выключена.\nЧтобы включить данную функцию, "
                       "купите pro-аккаунт в разделе `Баланс`."
            },
            "inline": {
                True: "Инлайн режим в режиме разработки.",
                False: "Инлайн режим в режиме разработки."
                #True: "Инлайн режим в вашем аккаунте включен.",
                #False: "Инлайн режим в вашем аккаунте выключен.\nЧтобы включить данную функцию, "
                #       "купите pro-аккаунт в разделе `Баланс`.\n"
                #       "Инлайн режим это возможность создания qr-кодов из других чатов."
            },
            "advertising": {
                True: "На данный момент реклама включена.\nЧтобы отключить рекламу, "
                      "купите pro-аккаунт в разделе `Баланс`.",
                False: "Реклама в вашем аккаунте отключена."
            },
            "balance": {
                "cancel": "Привет, оплаченое время pro-аккаунта закончилось, теперь вам доступны базовые функции."
                          "Чтобы продолжить пользоваться безлимитом, оплатите pro-аккаунт в разделе `Баланс`, "
                          "либо в том же разделе введите промокод.",
                True: "🥳Pro-аккаунт активирован.🥳 Оплачено дней: ",
                False: "😒*Pro-аккаунт отключен.*😒\nЕсли у вас есть промокод, нажмите на соответствующую кнобку в меню.\n"
                       "Чтобы купить pro-аккаунт, или узнать цену, нажмите на 'Купить pro-аккаунт'.\n"
                       "Если вы оплатили pro-аккаунт, но он не был активирован, нажмите 'Обновить'. "
                       "Если не поможет, напишите нам в службу поддержки.\n"
                       "Если вы хотите получить pro-аккаунт на специальных условиях(на неделю, со скидкой), обратитесь "
                       "к нам в службу поддержки с объяснением, "
                       "почему мы далжны выдать вам pro-аккаунт на специальных условиях."
            },
            "buy_pro": "Купить pro-аккаунт",
            "extend_pro": "Продлить pro-аккаунт",
            "promotion": "Отправте мне промокод✏(12 символов) без лишних символов.\n"
                         "Если вы хотите подарить pro-аккаунт другу, можете купить промокод на 1 месяц или на 1 год. "
                         "Для этого нажмите соответствующие кнопки в меню.\n"
                         "Промокод возврату и обмену не подлежит, действителен 30 дней с момента покупки.",
            "promo": {
                "newpromotion": "Создан промокод: ",
                "buymonth": "🎁Вы купили промокод на 1 месяц.\nВаш промокод: ",
                "buyyear": "🎁Вы купили промокод на 1 год.\nВаш промокод: ",
                True: "🎉Промокод успешно применен.🎉",
                False: "❌Промокод недействителен.❌\n"
                       "Если вы уверены в действительности промокода, или желаете узнать причину недействительности "
                       "промокода, напишите нам в службу поддержки.",
                "used1": "использовал ваш промокод:",
                "used2": "и он больше не действителен.",
                "cancel": "Прошел месяц от покупки вами промокода, к великому сожалению он больше не действителен, "
                          "так как его никто не использовал за месяц.\nПромокод:",
            },
            "buymonth": {
                "title": "1 месяц pro-аккаунта",
                "description": "Оплатив 1 месяц pro-аккаунта, у вас разблокируются все возможности, "
                               "не будет приходить реклама, в описании qr-кодов не будет рекламы этого бота, "
                               "а также лишних кнопок целый месяц.",
                "price": "Подписка на 1 месяц",
            },
            "buyyear": {
                "title": "1 год pro-аккаунта",
                "description": "Оплатив 1 год pro-аккаунта, у вас разблокируются все возможности, "
                               "не будет приходить реклама, в описании qr-кодов не будет рекламы этого бота, "
                               "а также лишних кнопок целый год.",
                "price": "Подписка на 1 год",
            },
            "admin_panel": "Вы зашли в панель администратора, выберите из меню что желаете сделать.",
            "allpost": "Введите текст, который отправить всем пользователям.",
            "nopropost": "Введите текст, который отправить всем пользователям, кроме владельцев pro-аккаунта.",
            "oneuserpost": "Пришлите мне chat-id пользователя, кому отправить пост.",
            "oneuserpost_text": "Введите текст, который отправить пользователю:\n",
            "statistic": "Статистика пользователей бота:",
            "background": "Выберите цвет из меню.\n"
                          "Текущий цвет фона:",
            "module": "Выберите цвет из меню.\n"
                      "Текущий цвет qr-кода:",
            "backgroundset": "Выбран цвет фона:",
            "moduleset": "Выбран цвет qr-кода:",
            "newpromotion": "Пришлите мне количество дней, на которое необходимо создать промокод."

        },
        "button": {
            "back": "🔙Назад",
            "cancel": "❌Отмена",
            "balance": "💳Баланс",
            "newqr": "➕Новый QR-код",
            "readqr": "📝Расшифровать QR-код",
            "newqrlogo": "🖼Новый QR-код с логотипом",
            "setting": "⚙Настройки",
            "support": "❗Помощь",
            "help": "ℹСправка",
            "language": "🇷🇺Язык",
            "color": "🔲Цвета поумолчанию",
            "inline": "📚Инлайн режим",
            "advertising": "📣Реклама",
            "buypro": "💰🔑Купить pro-аккаунт",
            "update": "🔄Обновить",
            "promotion": "🎁Ввести промокод",
            "extendpro": "💰Продлить pro-аккаунт",
            "buymonth": "📆Купить 1 месяц",
            "buyyear": "📅Купить 1 год",
            "buypromotionmonth": "📆Купить промокод на 1 месяц",
            "buypromotionyear": "📅Купить промокод на 1 год",
            "adminpanel": "🔐Панель администратора",
            "allpost": "Опубликовать пост всем",
            "nopropost": "Опубликовать пост не pro",
            "oneuserpost": "Отправить пост одному",
            "statistic": "Получить статистику",
            "newpromotion": "Создать промокод",
            "background": "◽Цвет фона",
            "module": "▪Цвет qr-кода",
            "logo": "🖼Поменять логотип",

        },
    }
}

color_array = [
    ("#FF0000", "🔴", "🟥"),
    ("#FFA500", "🟠", "🟧"),
    ("#FFFF00", "🟡", "🟨"),
    ("#008000", "🟢", "🟩"),
    ("#0000FF", "🔵", "🟦"),
    ("#DA70D6", "🟣", "🟪"),
    ("#000000", "⚫", "⬛"),
    ("#FFFFFF", "⚪", "⬜"),
    ("#A52A2A", "🟤", "🟫")
]