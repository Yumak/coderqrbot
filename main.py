import math
import time
import pyqrcode
import telebot
from PIL import Image
import json
import psycopg2
import random
from text import string, color_array
import base64
import requests
import threading
import schedule


TOKEN = "1318380133:AAGB2ITsJjKBt-o962V5tXH7Kl3seHZ7mfA"
SUPPORT_CHAT_ID = -1001178190059
ADMIN_CHAT_ID = 1093110311
TEST_PAYMENT_TOKEN = "381764678:TEST:20411"
PAYMENT_TOKEN = "390540012:LIVE:13322"
CLIENT_API_KEY = "be2c89966708c78132683c1289e5fed1"


bot = telebot.TeleBot(token=TOKEN, parse_mode="MARKDOWN")
conn = psycopg2.connect(dbname='coderqr',
                        user='coderqr',
                        password='123',
                        host='46.17.104.151')
conn.autocommit = True
cursor = conn.cursor()

def_color = ("#000000", "#FFFFFF")
to_send_one_post_user_id = 0


def bot_start():
    print("listen bot...")
    bot.polling(none_stop=True, interval=0, timeout=20)


def thread_start():
    print('thread start...')
    while True:
        if time.strftime("%H", time.localtime()) == "00" and time.strftime("%M", time.localtime()) == "00":
            every_day()
            time.sleep(3600)
        else:
            time.sleep(10)


def sendMessage(chat_id, text, reply_markup=None, reply_to_message_id=None):
    try:
        bot.send_chat_action(chat_id=chat_id, action='typing')
        msg = bot.send_message(chat_id=chat_id,
                               text=text,
                               reply_markup=reply_markup,
                               reply_to_message_id=None)
    except telebot.apihelper.ApiTelegramException as error:
        msg = error.args
        print(msg)


def every_day():
    sendMessage(ADMIN_CHAT_ID, "Запускаю ежедневную проверку")
    cursor.execute(f'SELECT * FROM users')
    users = cursor.fetchall()
    for user in users:
        if user[9] == False:
            cursor.execute(f'UPDATE users SET create_count_normal = %s, create_count_logo = %s, read_count = %s '
                           f'WHERE chat_id = {user[0]}',
                           (5, 3, 5))
        elif user[7] < time.time():
            cursor.execute(f'UPDATE users SET create_count_normal = %s, create_count_logo = %s, expiry_date_pro = %s,'
                           f'normal_color = %s, pro_account = %s, read_count = %s, advertising = %s, default_color = %s '
                           f'WHERE chat_id = {user[0]}',
                           (5, 3, 0, False, False, 5, True, def_color))
            sendMessage(chat_id=user[0],
                        text=string[user[4]]["message"]["balance"]["cancel"],
                        reply_markup=None)

        else:
            cursor.execute(f'UPDATE users SET create_count_normal = %s, create_count_inline = %s, read_count = %s '
                           f'WHERE chat_id = {user[0]}',
                           (5, 3, 5))
    cursor.execute(f'SELECT * FROM promotion')
    promotions = cursor.fetchall()
    for promotion in promotions:
        if promotion[4] < time.time():
            cursor.execute(f'UPDATE promotion SET status = {False} WHERE id = {promotion[0]}')
            cursor.execute(f'SELECT * FROM users WHERE chat_id = {promotion[6]}')
            data = cursor.fetchone()
            sendMessage(chat_id=promotion[6],
                        text=f'{string[data[4]]["message"]["pro-account"]["cancel"]} `{promotion[1]}`')


def get_promo_code(num_chars):
    code_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    code_true = False
    while code_true != True:
        code = ''
        for i in range(0, num_chars):
            slice_start = random.randint(0, len(code_chars) - 1)
            code += code_chars[slice_start: slice_start + 1]
        cursor.execute(f"SELECT * FROM promotion WHERE name_promotion = '{code}'")
        if cursor.fetchone() == None:
            code_true = True
    return code


def button_menu(data):
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["newqr"], string[data[4]]["button"]["readqr"])
    keyboard.row(string[data[4]]["button"]["newqrlogo"])
    keyboard.row(string[data[4]]["button"]["balance"], string[data[4]]["button"]["setting"])
    keyboard.row(string[data[4]]["button"]["support"], string[data[4]]["button"]["help"])
    return keyboard


def language(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    markup = telebot.types.InlineKeyboardMarkup()
    ru = telebot.types.InlineKeyboardButton(text='🇷🇺Русский', callback_data='ru')
    en = telebot.types.InlineKeyboardButton(text='🇺🇸English', callback_data='en')
    back = telebot.types.InlineKeyboardButton(text=string[data[4]]["button"]["back"], callback_data='back')
    markup.add(ru, en)
    if data[12] == "language":
        markup.add(back)
    sendMessage(chat_id=message.chat.id,
                text=string["lang"],
                reply_markup=markup)


def inline(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("inline",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["balance"])
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=string[data[4]]["message"]["inline"][data[8]],
                reply_markup=keyboard)


def advertising(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("advertising",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["balance"])
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=string[data[4]]["message"]["advertising"][data[11]],
                reply_markup=keyboard)


def color(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("color",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    if data[8]:
        keyboard.row(string[data[4]]["button"]["background"], string[data[4]]["button"]["module"])
    else:
        keyboard.row(string[data[4]]["button"]["balance"])
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=string[data[4]]["message"]["color"][data[8]],
                reply_markup=keyboard)


def background(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("background",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    this_color = data[13][9:16]
    for col in color_array:
        if col[0] == this_color:
            col_b = col[2]
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("🟥", "🟧", "🟨", "🟩", "🟦", "🟪", "⬛", "⬜", "🟫")
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=f'{string[data[4]]["message"]["background"]} {col_b}',
                reply_markup=keyboard)


def module(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("module",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    this_color = data[13][1:8]
    for col in color_array:
        if col[0] == this_color:
            col_m = col[1]
            break
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("🔴", "🟠", "🟡", "🟢", "🔵", "🟣", "⚫", "⚪", "🟤")
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=f'{string[data[4]]["message"]["module"]} {col_m}',
                reply_markup=keyboard)


def background_set(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    for col in color_array:
        if col[2] == message.text:
            col_b = col[0]
    colors = (data[13][1:8], col_b)
    cursor.execute(f'UPDATE users SET default_color = %s WHERE chat_id = {message.chat.id}',
                   (colors,))
    sendMessage(chat_id=message.chat.id,
                text=f'{string[data[4]]["message"]["backgroundset"]} {message.text}')
    color(message)


def module_set(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    for col in color_array:
        if col[1] == message.text:
            col_m = col[0]
    colors = (col_m, data[13][9:16])
    cursor.execute(f'UPDATE users SET default_color = %s WHERE chat_id = {message.chat.id}',
                   (colors,))
    sendMessage(chat_id=message.chat.id,
                text=f'{string[data[4]]["message"]["moduleset"]} {message.text}')
    color(message)


def buy_pro(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("buy_pro",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["buymonth"], string[data[4]]["button"]["buyyear"])
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=string[data[4]]["message"]["buy_pro"],
                reply_markup=keyboard)


def extend_pro(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("extend_pro",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["buymonth"], string[data[4]]["button"]["buyyear"])
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=string[data[4]]["message"]["extend_pro"],
                reply_markup=keyboard)


def promotion(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("promotion",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["buypromotionmonth"], string[data[4]]["button"]["buypromotionyear"])
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=string[data[4]]["message"]["promotion"],
                reply_markup=keyboard)


def admin_panel(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("paneladmin",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["allpost"], string[data[4]]["button"]["nopropost"])
    keyboard.row(string[data[4]]["button"]["oneuserpost"], string[data[4]]["button"]["statistic"])
    keyboard.row(string[data[4]]["button"]["newpromotion"])
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=ADMIN_CHAT_ID,
                text=string[data[4]]["message"]["admin_panel"],
                reply_markup=keyboard)


def allpost(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("allpost",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=ADMIN_CHAT_ID,
                text=string[data[4]]["message"]["allpost"],
                reply_markup=keyboard)


def nopropost(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("nopropost",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=ADMIN_CHAT_ID,
                text=string[data[4]]["message"]["nopropost"],
                reply_markup=keyboard)


def oneuserpost(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("oneuserpost",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text={string["russian"]["message"]["oneuserpost"]},
                reply_markup=keyboard)


def oneuserpost_text(message):
    if message.chat.id != ADMIN_CHAT_ID:
        return
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("oneuserpost_text",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {to_send_one_post_user_id}')
    data = cursor.fetchone()
    print(data)
    if not data:
        oneuserpost(message)
    else:
        sendMessage(chat_id=ADMIN_CHAT_ID,
                    text=f'{string["russian"]["message"]["oneuserpost_text"]}'
                         f'{data[1]} {data[2]} | @{data[3]} | `{data[0]}`')


def statistic(message):
    if message.chat.id != ADMIN_CHAT_ID:
        return
    bot.send_chat_action(chat_id=message.chat.id, action='upload_document')
    count_pro = 0
    count_user = 0
    cursor.execute(f'SELECT * FROM users')
    users = cursor.fetchall()
    with open('files/users.txt', 'w') as f:
        for user in users:
            f.write(f"{user[0]} | {user[1]} {user[2]} | {user[3]} | {user[4]}"
                    f" | pro: {user[9]}\n".encode('utf-8').decode('utf-8'))
            if user[9]:
                count_pro += 1
            count_user += 1
    cursor.execute(f'SELECT * FROM promotion')
    promotions = cursor.fetchall()
    with open('files/promotions.txt', 'w') as f:
        for promotion in promotions:
            f.write(f"{promotion[1]}|{promotion[3]}|{promotion[5]}|{promotion[6]}|{promotion[7]}\n")
    try:
        bot.send_media_group(chat_id=ADMIN_CHAT_ID,
                             media=[
                                 telebot.types.InputMediaDocument(media=open('files/users.txt', 'rb'),
                                                                  caption='Список пользователей☝\n'
                                                                          f'Всего пользователей: {count_user}\n'
                                                                          f'Пользователей с pro-аккаунтоами: {count_pro}\n'),
                                 telebot.types.InputMediaDocument(media=open('files/promotions.txt', 'rb'),
                                                                  caption='Список промокодов☝\n'),
                             ])
    except telebot.apihelper.ApiTelegramException as error:
        msg = error.args
        print(msg)
    admin_panel(message)


def newpromotion(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("newpromotion",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text={string["russian"]["message"]["newpromotion"]},
                reply_markup=keyboard)


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        if not data:
            cursor.execute(f'INSERT INTO users (chat_id, first_name, last_name, username, lang, create_count_normal, '
                           f'create_count_logo, expiry_date_pro, normal_color, read_count, '
                           f'pro_account, advertising, status, default_color, unlimited_pro) '
                           f'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                           (message.chat.id, str(message.from_user.first_name), str(message.from_user.last_name),
                            str(message.from_user.username), "russian", 5, 3, 0, False, 3, False, True, "start",
                            def_color, False))
            language(message)
        else:
            cursor.execute(f'UPDATE users SET first_name = %s, last_name = %s, username = %s,'
                           f'status = %s, default_color = %s WHERE chat_id = {message.chat.id}',
                           (str(message.from_user.first_name), str(message.from_user.last_name),
                            str(message.from_user.username), "start", def_color))
            cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
            data = cursor.fetchone()
            keyboard = button_menu(data)
            sendMessage(chat_id=message.chat.id,
                        text=string[data[4]]["message"]["restart"],
                        reply_markup=keyboard)


@bot.message_handler(commands=['help'])
def help(message):  # Calling a help message
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("help",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        keyboard.row(string[data[4]]["button"]["back"])
        sendMessage(chat_id=message.chat.id,
                    text=string[data[4]]["message"]["help"],
                    reply_markup=keyboard)


@bot.message_handler(commands=['newqr'])
def newqr(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("new_qr",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        if data[9] == True:
            attempts = '♾'
        else:
            attempts = data[5]
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        keyboard.row(string[data[4]]["button"]["back"])
        sendMessage(chat_id=message.chat.id,
                    text=f'{string[data[4]]["message"]["newqr"]} `{attempts}`',
                    reply_markup=keyboard)


@bot.message_handler(commands=['newqrlogo'])
def newqrlogo(message):
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("new_qr_logo",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        bot.send_chat_action(chat_id=message.chat.id, action='upload_photo')
        print(data)
        if data[9]:
            attempts = '♾'
        else:
            attempts = data[6]
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        keyboard.row(string[data[4]]["button"]["logo"])
        keyboard.row(string[data[4]]["button"]["back"])
        try:
            try:
                f = open(f'files/logo/{message.chat.id}.jpg', 'r')
                filename = message.chat.id
            except FileNotFoundError as error:
                filename = "default"
            msg = bot.send_photo(chat_id=message.chat.id,
                                 photo=Image.open(f'files/logo/{filename}.jpg'),
                                 caption=f'{string[data[4]]["message"]["newqrlogo"]} `{attempts}`',
                                 reply_markup=keyboard)
        except telebot.apihelper.ApiTelegramException as error:
            msg = error.args[0]


def changelogo(message):
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("change_logo",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    print(data)
    bot.send_chat_action(chat_id=message.chat.id, action='upload_photo')
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[4]]["button"]["back"])
    try:
        try:
            f = open(f'files/logo/{message.chat.id}.jpg', 'r')
            filename = message.chat.id
        except FileNotFoundError as error:
            filename = "default"
        msg = bot.send_photo(chat_id=message.chat.id,
                             photo=Image.open(f'files/logo/{filename}.jpg'),
                             caption=string[data[4]]["message"]["changelogo"],
                             reply_markup=keyboard)
    except telebot.apihelper.ApiTelegramException as error:
        msg = error.args[0]


@bot.message_handler(commands=['balance'])
def balance(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("balance",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        if data[14] == False:
            if data[9] == False:
                keyboard.row(string[data[4]]["button"]["buypro"], string[data[4]]["button"]["promotion"])
                keyboard.row(string[data[4]]["button"]["update"])
                text_balance = ''
            else:
                keyboard.row(string[data[4]]["button"]["extendpro"], string[data[4]]["button"]["promotion"])
                keyboard.row(string[data[4]]["button"]["update"])
                text_balance = f"{math.ceil((data[7] - time.time())/86400)}"
        keyboard.row(string[data[4]]["button"]["back"])
        sendMessage(chat_id=message.chat.id,
                    text=string[data[4]]["message"]["balance"][data[9]] + text_balance,
                    reply_markup=keyboard)


@bot.message_handler(commands=['readqr'])
def readqr(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("read_qr",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        keyboard.row(string[data[4]]["button"]["back"])
        if data[9]:
            attempts = '♾'
        else:
            attempts = data[10]
        sendMessage(chat_id=message.chat.id,
                    text=f'{string[data[4]]["message"]["readqr"]} {attempts}',
                    reply_markup=keyboard)


@bot.message_handler(commands=['setting'])
def setting(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("setting",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        keyboard.row(string[data[4]]["button"]["language"], string[data[4]]["button"]["color"])
        keyboard.row(string[data[4]]["button"]["inline"], string[data[4]]["button"]["advertising"])
        if message.chat.id == ADMIN_CHAT_ID:
            keyboard.row(string[data[4]]["button"]["adminpanel"])
        keyboard.row(string[data[4]]["button"]["back"])
        sendMessage(chat_id=message.chat.id,
                    text=string[data[4]]["message"]["setting"],
                    reply_markup=keyboard)


@bot.message_handler(commands=['cancel'])
def cancel(message):  # Cancel all actions. Returns to the home screen.
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("start",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = button_menu(data)
        sendMessage(chat_id=message.chat.id,
                    text=string[data[4]]["message"]["cancel"],
                    reply_markup=keyboard)


def back(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("start",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = button_menu(data)
        sendMessage(chat_id=message.chat.id,
                    text=string[data[4]]["message"]["back"],
                    reply_markup=keyboard)


@bot.message_handler(commands=['support'])
def support(message):  # User support service
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("support",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        keyboard.row(string[data[4]]["button"]["back"])
        sendMessage(chat_id=message.chat.id,
                    text=string[data[4]]["message"]["support"],
                    reply_markup=keyboard)


@bot.message_handler(content_types=['text'])
def message(message):
    #bot.send_chat_action(chat_id=message.chat.id, action='typing')
    global to_send_one_post_user_id
    if message.chat.id == SUPPORT_CHAT_ID:  # If messages come from the support service, we send them to the user
        cursor.execute(f"SELECT * FROM messages WHERE message_id = {message.reply_to_message.message_id};")
        data = cursor.fetchone()
        try:
            msg = bot.send_message(
                chat_id=data[0],
                reply_to_message_id=data[2],
                text=f"{message.text}")
            cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                           f"VALUES (0, {msg.message_id}, {message.message_id});")
        except telebot.apihelper.ApiTelegramException as error:
            msg = error.args[0]
    else:
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        if message.reply_to_message is not None:
            if message.reply_to_message.content_type == 'text':
                cursor.execute(f'SELECT * FROM messages WHERE from_id = 0 AND '
                               f'message_id = {message.reply_to_message.message_id};')
                data = cursor.fetchone()
                if data is not None:
                    try:
                        msg = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                               text=f"{message.from_user.first_name} {message.from_user.last_name}"
                                                    f" | @{message.from_user.username}\n"
                                                    f"chat-id = {message.chat.id}\n{message.text}",
                                               reply_to_message_id=data[2])
                        cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                       f"VALUES (%s, %s, %s)", (message.chat.id, msg.message_id, message.message_id,))
                    except telebot.apihelper.ApiTelegramException as error:
                        msg = error.args[0]
                        print(msg)
                else:
                    try:
                        msg = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                               text=f"`{message.reply_to_message.text}`\n\n"
                                                    f"{message.from_user.first_name} {message.from_user.last_name}"
                                                    f" | @{message.from_user.username}\n"
                                                    f"chat-id = {message.chat.id}\n{message.text}")
                        cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                       f"VALUES (%s, %s, %s)", (message.chat.id, msg.message_id, message.message_id,))
                    except telebot.apihelper.ApiTelegramException as error:
                        msg = error.args[0]
                        print(msg)
        if data[12] == 'start':
            if message.text == "➕New QR-code" or message.text == "➕Новый QR-код":
                newqr(message)
            elif message.text == "📝Decipher QR-code" or message.text == "📝Расшифровать QR-код":
                readqr(message)
            elif message.text == "🖼New QR-code with logo" or message.text == "🖼Новый QR-код с логотипом":
                newqrlogo(message)
            elif message.text == "💳Balance" or message.text == "💳Баланс":
                balance(message)
            elif message.text == "⚙Setting" or message.text == "⚙️Настройки" or message.text == "⚙Настройки"\
                    or message.text == "⚙️Setting":
                setting(message)
            elif message.text == "❗Support" or message.text == "❗Помощь":
                support(message)
            elif message.text == "ℹHelp" or message.text == "ℹСправка":
                help(message)
        elif data[12] == 'new_qr':
            if message.text == "🔙Back" or message.text == "🔙Назад":
                back(message)
            else:
                bot.send_chat_action(chat_id=message.chat.id, action='upload_photo')
                if data[9] == True or data[5] > 0:
                    create_qr(message)
                    if data[9] == False:
                        cursor.execute(f'UPDATE users SET create_count_normal = %s WHERE chat_id = {message.chat.id}',
                                       (data[5] - 1,))
                    newqr(message)
                else:
                    sendMessage(chat_id=message.chat.id,
                                text=f'{string[data[4]]["message"]["newqrrez"][False]}',
                                reply_to_message_id=message.message_id)
                    back(message)
        elif data[12] == 'read_qr':
            if message.text == "🔙Back" or message.text == "🔙Назад":
                back(message)
            else:
                readqr(message)
        elif data[12] == 'new_qr_logo':
            if message.text == "🔙Back" or message.text == "🔙Назад":
                back(message)
            elif message.text == "🖼Change logo" or message.text == "🖼Поменять логотип":
                changelogo(message)
            else:
                if data[9] == True or data[6] > 0:
                    bot.send_chat_action(chat_id=message.chat.id, action='upload_photo')
                    create_qr_logo(message)
                    if data[9] == False:
                        cursor.execute(f'UPDATE users SET create_count_logo = %s WHERE chat_id = {message.chat.id}',
                                       (data[6] - 1,))
                    newqrlogo(message)
                else:
                    sendMessage(chat_id=message.chat.id,
                                text=f'{string[data[4]]["message"]["newqrrez"][False]}',
                                reply_to_message_id=message.message_id)
                    back(message)
        elif data[12] == 'change_logo':
            if message.text == "🔙Back" or message.text == "🔙Назад":
                newqrlogo(message)
            else:
                changelogo(message)
        elif data[12] == 'balance':
            if message.text == "🔙Back" or message.text == "🔙Назад":
                back(message)
            elif message.text == "💰🔑Buy pro-account" or message.text == "💰🔑Купить pro-аккаунт":
                buy_pro(message)
            elif message.text == "🔄Update" or message.text == "🔄Обновить":
                balance(message)
            elif message.text == "💰Extend pro-account" or message.text == "💰Продлить pro-аккаунт":
                extend_pro(message)
            elif message.text == "🎁Enter a promo code" or message.text == "🎁Ввести промокод":
                promotion(message)
        elif data[12] == 'setting':
            if message.text == "🇺🇸Language" or message.text == "🇷🇺Язык":
                cursor.execute(f"UPDATE users SET status = 'language' WHERE chat_id = {message.chat.id}")
                language(message)
            elif message.text == "🔲Default color" or message.text == "🔲Цвета поумолчанию":
                color(message)
            elif message.text == "📚Inline mode" or message.text == "📚Инлайн режим":
                inline(message)
            elif message.text == "📣Advertising" or message.text == "📣Реклама":
                advertising(message)
            elif message.text == "🔐Admin panel" or message.text == "🔐Панель администратора":
                admin_panel(message)
            elif message.text == "🔙Back" or message.text == "🔙Назад":
                back(message)
        elif data[12] == 'support':  # User support service
            if message.text == "🔙Назад" or message.text == "Назад" or message.text == "🔙Back"\
                    or message.text == "Back":
                back(message)
            else:
                print(message)
                if message.reply_to_message == None:
                    try:
                        msg = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                               text=f"{message.from_user.first_name} {message.from_user.last_name}"
                                                    f" | @{message.from_user.username}\n"
                                                    f"chat-id = {message.chat.id}\n{message.text}",)
                        cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                       f"VALUES (%s, %s, %s)", (message.chat.id, msg.message_id, message.message_id,))
                    except telebot.apihelper.ApiTelegramException as error:
                        msg = error.args[0]
                        print(msg)
                else:
                    cursor.execute(f'SELECT * FROM messages WHERE from_id = 0 AND '
                                   f'message_id = {message.reply_to_message.message_id};')
                    data = cursor.fetchone()
                    try:
                        msg = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                               text=f"{message.from_user.first_name} {message.from_user.last_name}"
                                                    f" | @{message.from_user.username}\n"
                                                    f"chat-id = {message.chat.id}\n{message.text}",
                                               reply_to_message_id=data[2])
                        cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                       f"VALUES (%s, %s, %s)", (message.chat.id, msg.message_id, message.message_id,))
                    except telebot.apihelper.ApiTelegramException as error:
                        msg = error.args[0]
                        print(msg)
        elif data[12] == 'help':
            if message.text == "🔙Назад" or message.text == "Назад" or message.text == "🔙Back" \
                    or message.text == "Back":
                back(message)
        elif data[12] == 'language':
            bot.delete_message(chat_id=message.chat.id,
                               message_id=message.message_id)
        elif data[12] == 'color':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                setting(message)
            elif message.text == "💳Balance" or message.text == "💳Баланс":
                balance(message)
            elif message.text == "◽Background color" or message.text == "◽Цвет фона":
                background(message)
            elif message.text == "▪Color qr-code" or message.text == "▪Цвет qr-кода":
                module(message)
        elif data[12] == 'inline':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                setting(message)
            elif message.text == "💳Balance" or message.text == "💳Баланс":
                balance(message)
        elif data[12] == 'advertising':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                setting(message)
            elif message.text == "💳Balance" or message.text == "💳Баланс":
                balance(message)
        elif data[12] == 'buy_pro':
            bot.send_chat_action(chat_id=message.chat.id, action='typing')
            if message.text == "🔙Назад" or message.text == "🔙Back":
                balance(message)
            elif message.text == "📆Купить 1 месяц" or message.text == "📆Buy 1 month":
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[4]]["message"]["buymonth"]["title"],
                                 description=string[data[4]]["message"]["buymonth"]["description"],
                                 invoice_payload='buy-one-month',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-month',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                        label=string[data[4]]["message"]["buymonth"]["price"],
                                        amount=10000)],
                                 is_flexible=False)
            elif message.text == "📅Купить 1 год" or message.text == "📅Buy 1 year":
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[4]]["message"]["buyyear"]["title"],
                                 description=string[data[4]]["message"]["buyyear"]["description"],
                                 invoice_payload='buy-one-year',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-year',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                     label=string[data[4]]["message"]["buyyear"]["price"],
                                     amount=100000)],
                                 is_flexible=False)
        elif data[12] == 'extend_pro':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                balance(message)
            elif message.text == "📆Купить 1 месяц" or message.text == "📆Buy 1 month":
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[4]]["message"]["buymonth"]["title"],
                                 description=string[data[4]]["message"]["buymonth"]["description"],
                                 invoice_payload='extend-one-month',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-month',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                        label=string[data[4]]["message"]["buymonth"]["price"],
                                        amount=10000)],
                                 is_flexible=False)
            elif message.text == "📅Купить 1 год" or message.text == "📅Buy 1 year":
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[4]]["message"]["buyyear"]["title"],
                                 description=string[data[4]]["message"]["buyyear"]["description"],
                                 invoice_payload='extend-one-year',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-year',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                     label=string[data[4]]["message"]["buyyear"]["price"],
                                     amount=100000)],
                                 is_flexible=False)
        elif data[12] == 'promotion':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                balance(message)
            elif message.text == "📆Купить промокод на 1 месяц" or message.text == "📆Buy a promo-code for 1 month":
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[4]]["message"]["buymonth"]["title"],
                                 description=string[data[4]]["message"]["buymonth"]["description"],
                                 invoice_payload='buy-promotion-one-month',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-month',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                        label=string[data[4]]["message"]["buymonth"]["price"],
                                        amount=10000)],
                                 is_flexible=False)
            elif message.text == "📅Купить промокод на 1 год" or message.text == "📅Buy a promo-code for 1 year":
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[4]]["message"]["buyyear"]["title"],
                                 description=string[data[4]]["message"]["buyyear"]["description"],
                                 invoice_payload='buy-promotion-one-year',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-year',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                     label=string[data[4]]["message"]["buyyear"]["price"],
                                     amount=100000)],
                                 is_flexible=False)
            else:
                cursor.execute(f"SELECT * FROM promotion WHERE name_promotion = '{message.text}'")
                code = cursor.fetchone()
                if not code:
                    sendMessage(chat_id=message.chat.id,
                                text=string[data[4]]["message"]["promo"][False])
                    balance(message)
                elif code[5] == False or code[4] < time.time():
                    sendMessage(chat_id=message.chat.id,
                                text=string[data[4]]["message"]["promo"][False])
                    balance(message)
                else:
                    if data[7] == 0:
                        expiry_date_pro = time.time() + code[3] * 86400
                    else:
                        expiry_date_pro = data[7] + code[3] * 86400
                    cursor.execute(f'UPDATE users SET expiry_date_pro = %s, normal_color = %s, pro_account = %s,'
                                   f'advertising = %s WHERE chat_id = {message.chat.id}',
                                   (expiry_date_pro, True, True, False,))
                    cursor.execute(f'UPDATE promotion SET status = %s, used_user_id = %s WHERE id = {code[0]}',
                                   (False, data[0]))
                    sendMessage(chat_id=message.chat.id,
                                text=string[data[4]]["message"]["promo"][True])
                    sendMessage(chat_id=code[6],
                                text=f'{data[1]} {data[2]} {string[data[4]]["message"]["promo"]["used1"]} '
                                     f'`{code[1]}`')
                    balance(message)
        elif data[12] == 'paneladmin':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                setting(message)
            elif message.text == "Опубликовать пост всем":
                allpost(message)
            elif message.text == "Опубликовать пост не pro":
                nopropost(message)
            elif message.text == "Отправить пост одному":
                oneuserpost(message)
            elif message.text == "Получить статистику":
                statistic(message)
            elif message.text == "Создать промокод":
                newpromotion(message)
        elif data[12] == 'allpost':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                admin_panel(message)
            else:
                cursor.execute(f'SELECT * FROM users')
                users = cursor.fetchall()
                for user in users:
                    sendMessage(chat_id=user[0],
                                text=f'*От администраторов:*\n{message.text}')
                admin_panel(message)
        elif data[12] == 'nopropost':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                admin_panel(message)
            else:
                cursor.execute(f'SELECT * FROM users WHERE pro_account = {False}')
                users = cursor.fetchall()
                for user in users:
                    sendMessage(chat_id=user[0],
                                text=f'*От администраторов:*\n{message.text}')
                admin_panel(message)
        elif data[12] == 'oneuserpost':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                admin_panel(message)
            else:
                try:
                    to_send_one_post_user_id = int(message.text)
                except ValueError as error:
                    pass
                if to_send_one_post_user_id > 0:
                    oneuserpost_text(message)
                else:
                    oneuserpost(message)
        elif data[12] == 'oneuserpost_text':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                oneuserpost(message)
            else:
                sendMessage(chat_id=to_send_one_post_user_id,
                            text=f'*От администраторов:*\n{message.text}')
                to_send_one_post_user_id = 0
                admin_panel(message)
        elif data[12] == 'background':
            if message.text == "🔙Back" or message.text == "🔙Назад":
                color(message)
            elif message.text == "🟥":
                background_set(message)
            elif message.text == "🟧":
                background_set(message)
            elif message.text == "🟨":
                background_set(message)
            elif message.text == "🟩":
                background_set(message)
            elif message.text == "🟦":
                background_set(message)
            elif message.text == "🟪":
                background_set(message)
            elif message.text == "⬛":
                background_set(message)
            elif message.text == "⬜":
                background_set(message)
            elif message.text == "🟫":
                background_set(message)
        elif data[12] == 'module':
            if message.text == "🔙Back" or message.text == "🔙Назад":
                color(message)
            elif message.text == "🔴":
                module_set(message)
            elif message.text == "🟠":
                module_set(message)
            elif message.text == "🟡":
                module_set(message)
            elif message.text == "🟢":
                module_set(message)
            elif message.text == "🔵":
                module_set(message)
            elif message.text == "🟣":
                module_set(message)
            elif message.text == "⚫":
                module_set(message)
            elif message.text == "⚪":
                module_set(message)
            elif message.text == "🟤":
                module_set(message)
        elif data[12] == 'newpromotion':
            if message.text == "🔙Назад" or message.text == "🔙Back":
                admin_panel(message)
            else:
                try:
                    day = int(message.text)
                except ValueError as error:
                    newpromotion(message)
                if day != None:
                    code = get_promo_code(12)
                    date_of_action = time.time() + 2592000
                    cursor.execute(
                        f'INSERT INTO promotion (name_promotion, pro_account, count_days, date_of_action, status, '
                        f'create_user_id) VALUES (%s, %s, %s, %s, %s, %s)',
                        (code, True, day, date_of_action, True, message.from_user.id))
                    sendMessage(chat_id=message.chat.id,
                                text=f'{string[data[4]]["message"]["promo"]["newpromotion"]} `{code}`',
                                reply_to_message_id=message.message_id)
                    admin_panel(message)


@bot.message_handler(content_types=['photo'])
def message_photo(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id == SUPPORT_CHAT_ID or message.chat.type != 'private':
        return
    else:
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        if data[12] == 'read_qr':
            if data[9] == True or data[10] > 0:
                res = decode(message)
                if res[0]["symbol"][0]["error"] == None:
                    text = res[0]["symbol"][0]["data"]
                else:
                    text = string[data[4]]["message"]["readqrrez"][False]
                sendMessage(chat_id=message.chat.id,
                            text=f'{string[data[4]]["message"]["readqrrez"]["text"]}`{text}`',
                            reply_to_message_id=message.message_id)
                if data[9] == False:
                    cursor.execute(f'UPDATE users SET read_count = %s WHERE chat_id = {message.chat.id}', (data[10] - 1,))
                readqr(message)
            else:
                sendMessage(chat_id=message.chat.id,
                            text=f'{string[data[4]]["message"]["readqrrez"]["cancel"]}',
                            reply_to_message_id=message.message_id)
                back(message)
        elif data[12] == 'change_logo':
            file_info = bot.get_file(message.photo[len(message.photo) - 1].file_id)
            d_f = bot.download_file(file_info.file_path)
            with open(f'files/logo/{message.chat.id}.jpg', 'wb') as f:
                f.write(d_f)
            newqrlogo(message)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    bot.send_chat_action(chat_id=call.message.chat.id, action='typing')
    cursor.execute(f"SELECT * FROM users WHERE chat_id = {call.message.chat.id}")
    data = cursor.fetchone()
    if call.data == 'ru':
        if data[12] == "start":
            bot.answer_callback_query(callback_query_id=call.id, text='Установлен язык: Русский')
            cursor.execute(f"UPDATE users SET lang = 'russian' WHERE chat_id = {call.message.chat.id}")
            keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
            keyboard.row("➕Новый QR-код", "📝Расшифровать QR-код")
            keyboard.row("🖼Новый QR-код с логотипом")
            keyboard.row("💳Баланс", "⚙️Настройки")
            keyboard.row("❗️Помощь", "ℹ️Справка")
            try:
                bot.delete_message(chat_id=call.message.chat.id,
                                   message_id=call.message.message_id)
                msg = bot.send_message(chat_id=call.message.chat.id,
                                       text=f"Привет, *{call.message.chat.first_name}*, "
                                            f"этот бот создан для создания и чтения "
                                            f"qr-кодов, выбери из меню что тебе нужно.👇🏽",
                                       reply_markup=keyboard)
            except telebot.apihelper.ApiTelegramException as error:
                msg = error.args[0]
        elif data[12] == "language":
            bot.answer_callback_query(callback_query_id=call.id, text='Установлен язык: Русский')
            bot.delete_message(chat_id=call.message.chat.id,
                               message_id=call.message.message_id)
            cursor.execute(f"UPDATE users SET lang = 'russian' WHERE chat_id = {call.message.chat.id}")
            setting(call.message)
    elif call.data == 'en':
        if data[12] == "start":
            bot.answer_callback_query(callback_query_id=call.id, text='Installed language: English')
            cursor.execute(f"UPDATE users SET lang = 'english' WHERE chat_id = {call.message.chat.id}")
            keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
            keyboard.row("➕New QR-code", "📝Decipher QR-code")
            keyboard.row("🖼New QR-code with logo")
            keyboard.row("💳Balance", "⚙Setting")
            keyboard.row("❗️Support", "ℹ️Help")
            try:
                bot.delete_message(chat_id=call.message.chat.id,
                                   message_id=call.message.message_id)
                msg = bot.send_message(chat_id=call.message.chat.id,
                                       text=f"Hi, *{call.message.chat.first_name}*, "
                                            f"this bot is designed to create and "
                                            f"read qr codes, choose from the menu what you need.👇🏽",
                                       reply_markup=keyboard)
            except telebot.apihelper.ApiTelegramException as error:
                msg = error.args[0]
        elif data[12] == "language":
            bot.answer_callback_query(callback_query_id=call.id, text='Установлен язык: Русский')
            bot.delete_message(chat_id=call.message.chat.id,
                               message_id=call.message.message_id)
            cursor.execute(f"UPDATE users SET lang = 'english' WHERE chat_id = {call.message.chat.id}")
            setting(call.message)
    elif call.data == 'back':
        if data[12] == "language":
            try:
                bot.delete_message(chat_id=call.message.chat.id,
                                   message_id=call.message.message_id)
            except telebot.apihelper.ApiTelegramException as error:
                pass
            setting(call.message)


def decode(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    print(message)
    file_info = bot.get_file(message.photo[len(message.photo) - 1].file_id)
    downloaded_file = bot.download_file(file_info.file_path)
    image_64 = base64.encodebytes(downloaded_file)
    r = requests.post(url="https://api.imgbb.com/1/upload",
                      data={
                           'key': CLIENT_API_KEY,
                           'expiration': 60,
                           'image': image_64
                      })
    response = json.loads(r.text)
    url = response['data']['url']
    qd = requests.get(url="http://api.qrserver.com/v1/read-qr-code/",
                       params={
                           'fileurl': url
                       })
    qdj = json.loads(qd.text)
    print(qdj)
    return qdj


@bot.pre_checkout_query_handler(func=lambda query: True)
def checkout(pre_checkout_query):
    print(pre_checkout_query)
    bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True)


@bot.message_handler(content_types=['successful_payment'])
def got_payment(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    print(message.successful_payment.invoice_payload)
    if message.successful_payment.invoice_payload == 'buy-one-year':
        if data[7] == 0:
            expiry_date_pro = time.time() + 31536000
        else:
            expiry_date_pro = data[7] + 31536000
        cursor.execute(f'UPDATE users SET expiry_date_pro = %s, normal_color = %s, pro_account = %s,'
                       f'advertising = %s WHERE chat_id = {message.chat.id}', (expiry_date_pro, True, True, False,))
        balance(message)
    elif message.successful_payment.invoice_payload == 'buy-one-month':
        if data[7] == 0:
            expiry_date_pro = time.time() + 2678400
        else:
            expiry_date_pro = data[7] + 2678400
        cursor.execute(f'UPDATE users SET expiry_date_pro = %s, normal_color = %s, pro_account = %s,'
                       f'advertising = %s WHERE chat_id = {message.chat.id}', (expiry_date_pro, True, True, False,))
        balance(message)
    if message.successful_payment.invoice_payload == 'extend-one-year':
        if data[7] == 0:
            expiry_date_pro = time.time() + 31536000
        else:
            expiry_date_pro = data[7] + 31536000
        cursor.execute(f'UPDATE users SET expiry_date_pro = %s, normal_color = %s, pro_account = %s,'
                       f'advertising = %s WHERE chat_id = {message.chat.id}', (expiry_date_pro, True, True, False,))
        balance(message)
    elif message.successful_payment.invoice_payload == 'extend-one-month':
        if data[7] == 0:
            expiry_date_pro = time.time() + 2678400
        else:
            expiry_date_pro = data[7] + 2678400
        cursor.execute(f'UPDATE users SET expiry_date_pro = %s, normal_color = %s, pro_account = %s,'
                       f'advertising = %s WHERE chat_id = {message.chat.id}', (expiry_date_pro, True, True, False,))
        balance(message)
    if message.successful_payment.invoice_payload == 'buy-promotion-one-year':
        code = get_promo_code(12)
        date_of_action = time.time() + 2592000
        cursor.execute(f'INSERT INTO promotion (name_promotion, pro_account, count_days, date_of_action, status, '
                       f'create_user_id) VALUES (%s, %s, %s, %s, %s, %s)',
                       (code, True, 365, date_of_action, True, message.from_user.id))
        sendMessage(chat_id=message.chat.id,
                    text=f'{string[data[4]]["message"]["promo"]["buyyear"]} `{code}`')
        promotion(message)
    elif message.successful_payment.invoice_payload == 'buy-promotion-one-month':
        code = get_promo_code(12)
        date_of_action = time.time() + 2592000
        cursor.execute(f'INSERT INTO promotion (name_promotion, pro_account, count_days, date_of_action, status, '
                       f'create_user_id) VALUES (%s, %s, %s, %s, %s, %s)',
                       (code, True, 30, date_of_action, True, message.from_user.id))
        sendMessage(chat_id=message.chat.id,
                    text=f'{string[data[4]]["message"]["promo"]["buymonth"]} `{code}`')
        promotion(message)


def create_qr(message):
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    text = f"{message.text}".encode('utf-8')
    try:
        qrcode = pyqrcode.QRCode(content=text, error='H')
        colors = data[13]
        with open(f'files/qrcode/{message.chat.id}.png', 'wb') as f:
            qrcode.png(f, scale=5, module_color=colors[1:8], background=colors[9:16])
        if data[9] == True:
            caption = ""
            keyboard = None
        else:
            caption = f'`{text}` \n{string[data[4]]["message"]["newqrrez"][True]}'
            keyboard = telebot.types.InlineKeyboardMarkup()
            promotion = telebot.types.InlineKeyboardButton(text='♥qr-coder♥', url="https://t.me/coderqrbot")
            keyboard.add(promotion)
        bot.send_photo(chat_id=message.chat.id,
                       photo=Image.open(f'files/qrcode/{message.chat.id}.png'),
                       caption=caption,
                       reply_to_message_id=message.message_id,
                       reply_markup=keyboard)
    except UnicodeDecodeError as error:
        print(error)
        sendMessage(chat_id=message.chat.id,
                    text="Не удалось зашифровать",
                    reply_to_message_id=message.message_id)


def create_qr_logo(message):
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    text = f"{message.text}".encode('utf-8')
    qrcode = pyqrcode.QRCode(text, error='H')
    colors = data[13]
    with open(f'files/qrcodelogo/{message.chat.id}.png', 'wb') as f:
        qrcode.png(f, scale=10, module_color=colors[1:8], background=colors[9:16])
    img = Image.open(f'files/qrcodelogo/{message.chat.id}.png')
    width, height = img.size

    try:
        logo = Image.open(f'files/logo/{message.chat.id}.jpg')
    except FileNotFoundError as error:
        logo = Image.open(f'files/logo/default.jpg')
    logo_width, logo_height = logo.size
    if logo_width > logo_height:
        k = logo_width / (width // 3)
    else:
        k = logo_height / (height // 3)
    logo_width_ok = logo_width / k
    logo_height_ok = logo_height / k

    # Calculate xmin, ymin, xmax, ymax to put the logo
    xmin = int((width / 2) - (logo_width_ok / 2))
    ymin = int((height / 2) - (logo_height_ok / 2))
    xmax = int((width / 2) + (logo_width_ok / 2))
    ymax = int((height / 2) + (logo_height_ok / 2))

    # resize the logo as calculated
    logo = logo.resize((xmax - xmin, ymax - ymin))

    # put the logo in the qr code
    img.paste(logo, (xmin, ymin, xmax, ymax))

    img.save(f'files/qrcodelogo/{message.chat.id}.png')

    if data[9]:
        caption = ""
        keyboard = None
    else:
        caption = f'`{text}` \n{string[data[4]]["message"]["newqrrez"][True]}'
        keyboard = telebot.types.InlineKeyboardMarkup()
        promotion = telebot.types.InlineKeyboardButton(text='♥qr-coder♥', url="https://t.me/coderqrbot")
        keyboard.add(promotion)
    bot.send_photo(chat_id=message.chat.id,
                   photo=Image.open(f'files/qrcodelogo/{message.chat.id}.png'),
                   caption=caption,
                   reply_to_message_id=message.message_id,
                   reply_markup=keyboard)


if __name__ == '__main__':
    thread1 = threading.Thread(target=thread_start)
    thread2 = threading.Thread(target=bot_start)
    thread1.start()
    thread2.start()
